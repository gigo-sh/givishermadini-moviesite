let movies = [];


function rendermovies(products) {
 const movielist = document.getElementById('productlist')
 movielist.innerHTML ='';
 let productsHTML ='';
 products.forEach(movie => {
     productsHTML +=`
     <div class="movie-card">
        <div class="movie-img">
            <img src=${movie.img}  alt="">
        </div>     
        <div class="movie-det">
            <p class="title">${movie.title} </p>
            <p class="description">${movie.description}</p>
            <div class="card-footer">
                <p class="genre">${movie.genre}</p>
                <p class="price">${movie.price}</p>
                <button class="buy"> Buy </button>
            </div>

        </div>   
    </div>
     `
 });
 movielist.insertAdjacentHTML('beforeend', productsHTML);

} 

window.onload = function() {
    
    fetch('../../db.json')
    .then(res => res.json())
    .then(data => {
        if(data) {
            rendermovies(data.movies)
        }
    }) 
}


